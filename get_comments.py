from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

chrome_path = './chromedriver'
#url = 'https://tw.yahoo.com/'
url = 'https://www.google.com/maps/place/%E8%BB%8A%E5%BA%AB%E9%B9%BD%E9%85%A5%E9%9B%9E/@24.8247746,121.0239369,17.06z/data=!4m7!3m6!1s0x0:0xc48e32f8552c143e!8m2!3d24.8247536!4d121.026028!9m1!1b1'
WAIT_TIME = 5

def extract_comments(web, SCROLL_LIMIT=None):

    div = web.find_element_by_css_selector('div.section-listbox.section-scrollbox.scrollable-y.scrollable-show')

    num_txts0 = 0
    i = 0
    while True:
        div.send_keys(Keys.END) #Keys.PAGE_DOWN
        txts = web.find_elements_by_css_selector('span.section-review-text')
        num_txts1 = len(txts)
        #print('the {}th scroll: {} reviews'.format(i+1, num_txts1))
        if (num_txts1 == num_txts0) or (SCROLL_LIMIT and i == SCROLL_LIMIT):
            break
        num_txts0 = num_txts1
        i += 1
        time.sleep(WAIT_TIME)

    btns = web.find_elements_by_css_selector('button.section-expand-review.blue-link')
    print('Total: {} expand buttons'.format(len(btns)))
    for b in btns:
        b.click()
    
    txts = web.find_elements_by_css_selector('span.section-review-text')
    print('Total: {} reviews'.format(len(txts)))
    ret = [t.text for t in txts]
    #for t in ret:
    #    print(t)
    return ret

web = webdriver.Chrome(chrome_path)
web.get(url)
time.sleep(WAIT_TIME)
#text = web.find_element_by_css_selector('input.gm2-hairline-border.gm2-body-1.jVMscDilb9d__input')
#text.send_keys("Mike Wu")
extract_comments(web)

#web.close()
